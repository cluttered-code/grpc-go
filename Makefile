all: clean build

build: proto build_server build_client

build_server:
	@echo "==> Building Server"
	go build -o ./cmd/server -i ./cmd/server

build_client:
	@echo "==> Building Client"
	go build -o ./cmd/client -i ./cmd/client

clean: clean_client clean_server clean_proto

clean_server:
	@echo "==> Cleaning Server"
	rm ./cmd/server/server

clean_client:
	@echo "==> Cleaning Client"
	rm ./cmd/client/client

clean_proto:
	@echo "==> Cleaning Protobufs"
	rm -r ./api/proto/*.pb.go

.PHONY: proto
proto:
	@echo "==> Generating Protobufs"
	protoc -I api/proto api/proto/quotes.proto --go_out=plugins=grpc:api/proto

