package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	quotes "gitlab.com/cluttered-code/grpc-go/api/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	"io"
	"log"
	"os"
	"reflect"
	"time"
)

const (
	address = "localhost:42069"
)

func parseArgs() (uint32, quotes.QuoteSource, error) {
	var count uint
	flag.UintVar(&count, "count", 10, "number of quotes (default: 10)")

	var source string
	flag.StringVar(&source, "source", "COMMUNITY", "source of the quotes (default: COMMUNITY)")

	flag.Parse()

	quoteSource, err := verifyArgs(source)

	return uint32(count), quoteSource, err
}

func verifyArgs(source string) (quotes.QuoteSource, error) {
	sourceValue, ok := quotes.QuoteSource_value[source]
	if ok != true {
		errStr := fmt.Sprintf("invalid source: %s (possible: %s)", source, reflect.ValueOf(quotes.QuoteSource_value).MapKeys())
		return quotes.QuoteSource_COMMUNITY, errors.New(errStr)
	}
	quoteSource := quotes.QuoteSource(sourceValue)

	return quoteSource, nil
}

func processRequest(source quotes.QuoteSource, count uint32) bool {
	conn, client, err := connect()
	if err != nil {
		log.Fatalf("failed to connect: %v", err)
		os.Exit(2)
	}
	defer conn.Close()

	request := &quotes.Request{
		Source: source,
		Count:  count,
	}
	done := streamRequest(client, request)
	return done
}

func connect() (*grpc.ClientConn, quotes.QuotesClient, error) {
	log.Println("Connecting to server...")
	params := grpc.ConnectParams{
		Backoff:           backoff.DefaultConfig,
		MinConnectTimeout: 10 * time.Second,
	}
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithConnectParams(params))
	if err != nil {
		return nil, nil, err
	}

	client := quotes.NewQuotesClient(conn)

	return conn, client, nil
}

func staticRequest(client quotes.QuotesClient, request *quotes.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	response, err := client.Quotes(ctx, request)
	if err != nil {
		log.Fatalf("failure response: %v", err)
	}

	log.Printf("Response: source=%s, quotes=%s", response.GetSource(), response.GetQuotes())
}

func streamRequest(client quotes.QuotesClient, request *quotes.Request) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	done := true

	log.Printf("Streaming Request")

	response, err := client.QuoteStream(ctx, request)
	if err != nil {
		log.Fatal(err)
		os.Exit(3)
	}

	for {
		quote, err := response.Recv()
		if err == io.EOF {
			log.Println("EOF")
			break
		}

		if response.Context().Err() != nil {
			log.Println(response.Context().Err())
			log.Println("RETRY PLEASE")
			done = false
			break
		}

		if err != nil {
			log.Fatalf("received error: %s", err)
			break
		}
		log.Println(quote.GetQuote())
	}

	return done
}

func main() {
	fmt.Println("######## Client ########")

	count, source, err := parseArgs()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	done := false
	for !done {
		done = processRequest(source, count)
	}

}
