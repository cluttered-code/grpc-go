package main

import (
	"fmt"
	quotes "gitlab.com/cluttered-code/grpc-go/api/proto"
	"gitlab.com/cluttered-code/grpc-go/internal/server"
	"google.golang.org/grpc"
	"log"
	"net"
)

const (
	port = ":42069"
)

func main() {
	fmt.Println("######## Quote Server ########")

	listener, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	quotes.RegisterQuotesServer(grpcServer, server.NewServer())
	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	} else {
		log.Printf("listening")
	}
}
