package generator

import (
	"math/rand"
	"time"
)

type QuoteGenerator struct {
	quotes []string
}

func NewCommunityQuotesGenerator() *QuoteGenerator {
	return &QuoteGenerator{
		quotes: communityQuotes,
	}
}

func NewOfficeQuotesGenerator() *QuoteGenerator {
	return &QuoteGenerator{
		quotes: officeQuotes,
	}
}

func (g *QuoteGenerator) RandomQuote() string {
	rand.Seed(time.Now().UnixNano())
	return g.quotes[rand.Intn(len(g.quotes)-1)]
}
