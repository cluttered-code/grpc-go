package server

import (
	"context"
	"fmt"
	quotes "gitlab.com/cluttered-code/grpc-go/api/proto"
	"gitlab.com/cluttered-code/grpc-go/internal/generator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"time"
)

type quoteServer struct {
	quotes.UnimplementedQuotesServer

	community *generator.QuoteGenerator
	office    *generator.QuoteGenerator
}

func (s *quoteServer) Quotes(ctx context.Context, request *quotes.Request) (*quotes.ResponseList, error) {
	log.Printf("Static Request: source=%s, count=%d", request.GetSource(), request.GetCount())

	quoteList := []string{}
	for i := uint32(0); i < request.GetCount(); i++ {
		quote := s.getQuote(request.GetSource())
		quoteList = append(quoteList, quote)
	}
	response := &quotes.ResponseList{
		Source: request.Source,
		Quotes: quoteList,
	}
	return response, nil
}

func (s *quoteServer) QuoteStream(request *quotes.Request, stream quotes.Quotes_QuoteStreamServer) error {
	log.Printf("Stream Request: source=%s, count=%d", request.GetSource(), request.GetCount())

	source := request.GetSource()

	for i := uint32(0); i < request.GetCount(); i++ {
		select {
		case <-time.After(time.Second):
			response := &quotes.Response{
				Source: source,
				Quote:  s.getQuote(source),
			}
			err := stream.Send(response)
			if err != nil {
				if status.Code(err) == codes.Unavailable {
					log.Println("client disconnected, exiting")
					return fmt.Errorf("client disconnected, exitiong: %w", err)
				}
			}
		}
	}
	return nil
}

func (s *quoteServer) getQuote(source quotes.QuoteSource) string {
	switch source {
	case quotes.QuoteSource_COMMUNITY:
		return s.community.RandomQuote()
	case quotes.QuoteSource_OFFICE:
		return s.office.RandomQuote()
	default:
		return ""
	}
}

func NewServer() *quoteServer {
	server := &quoteServer{
		community: generator.NewCommunityQuotesGenerator(),
		office:    generator.NewOfficeQuotesGenerator(),
	}

	return server
}
